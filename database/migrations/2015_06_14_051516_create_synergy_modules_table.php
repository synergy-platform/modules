<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSynergyModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {

			$table->engine = 'InnoDB';

			$table->increments('id');
            $table->timestamps();
			$table->string('slug')->unique();
			$table->string('version', 50);
			$table->boolean('installed');
			$table->boolean('enabled');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modules');
    }
}
