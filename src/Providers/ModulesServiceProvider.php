<?php

namespace Synergy\Modules\Providers;

use Synergy\Modules\Finder;
use Synergy\Modules\Modules;
use Synergy\Modules\Repositories\ModuleRepository;
use Synergy\Modules\Validators\ModuleValidator;
use Synergy\Support\ServiceProvider;
use Symfony\Component\Finder\Finder as SymfonyFinder;

/**
 * Part of the Modules package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Modules
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/modules
 */

class ModulesServiceProvider extends ServiceProvider
{

	/**
	 * Boots the packages resources
	 */
	public function boot()
	{
		$app = $this->app;

		$this->bootResources();

		if (config('synergy-modules.register')) {

			$this->app['modules']->findAndRegister(true);

			foreach($app['modules']->all() as $module) {

				if ( ! $module->isInstalled()) {
					$module->install();
				}
			}

			foreach($app['modules']->allEnabled() as $module) {
				$module->boot();
			}
		}
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerValidator();
		$this->registerFinder();
		$this->registerRepository();
		$this->registerModules();
	}

	/**
	 * Registers the module finder with the container
	 */
	protected function registerFinder()
	{
		$this->app->singleton('modules.finder', function($app) {

			$paths = config('synergy-modules.paths', []);

			$finder =new Finder(new SymfonyFinder, $paths);

			$finder->setManifest(config('synergy-modules.manifest.name'));
			$finder->setDepth(config('synergy-modules.manifest.depth'));

			return $finder;
		});
	}

	/**
	 * Registers the Modules class
	 */
	protected function registerModules()
	{
		$this->app->singleton('modules', function ($app) {

			return new Modules;
		});
	}

	/**
	 * Registers the module repository with the container
	 */
	protected function registerRepository()
	{
		$this->app->singleton('modules.repository', function($app) {
			return new ModuleRepository($app['modules.validator']);
		});
	}

	/**
	 * Registers the module validation service with the container
	 */
	protected function registerValidator()
	{
		$this->app->singleton('modules.validator', function($app) {

			return new ModuleValidator($app['validator']);
		});
	}

	/**
	 * Boots the packages resources (translations, config, views, etc...)
	 */
	protected function bootResources()
	{
		// Publish config
		$config = realpath(__DIR__ . '/../../config/config.php');

		$this->mergeConfigFrom($config, 'synergy-modules');

		$this->publishes([
			$config => config_path('synergy-modules.php'),
		], 'config');

		// Publish migrations
		$migrations = realpath(__DIR__ . '/../../database/migrations');

		$this->publishes([
			$migrations => $this->app->databasePath().'/migrations',
		], 'migrations');
	}

}