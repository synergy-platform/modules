<?php

namespace Synergy\Modules;

use Closure;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use RuntimeException;
use Synergy\Dependable\Dependable;
use Synergy\Modules\Repositories\ModuleRepository;
use StdClass;

/**
 * Part of the Modules package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Modules
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/modules
 */

class Module implements Dependable, Arrayable, Jsonable
{

	/**
	 * Instance of the Module repository
	 *
	 * @var ModuleRepository
	 */
	protected $repository;

	/**
	 * Base path to the module
	 *
	 * @var string
	 */
	protected $path;

	/**
	 * Module's unique slug, used as an identifier
	 *
	 * @var string
	 */
	protected $slug;

	/**
	 * Array containing module's manifest attributes
	 *
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * Returns the Eloquent entity for the module
	 *
	 * @var string
	 */
	protected $entity;

	/**
	 * Flag for whether the module has been registered.
	 *
	 * @var bool
	 */
	protected $registered = false;

	/**
	 * Flag for whether the module has been booted.
	 *
	 * @var bool
	 */
	protected $booted = false;


	/**
	 * Creates a new instance of the Module class
	 *
	 * @param ModuleRepository $repository
	 * @param                  $path
	 * @param                  $slug
	 * @param array            $attributes
	 */
	function __construct(ModuleRepository $repository, $path, $slug, array $attributes)
	{
		$this->repository = $repository;

		$this->path = $path;

		$this->slug = $slug;

		// setup our attributes
		$this->setAttributes($attributes);

		// set the model instance
		$this->entity = $this->repository->findBySlug($this->slug);
	}

	/**
	 * Registers the module
	 */
	public function register()
	{
		if (isset($this->attributes['providers'])) {
			foreach ($this->attributes['providers'] as $provider) {
				app()->resolveProviderClass($provider)->register();
			}
		}

		$this->registered = true;
	}

	/**
	 * Registers the module's service providers
	 */
	protected function registerProviders()
	{
		if (isset($this->attributes['providers'])) {
			foreach ($this->attributes['providers'] as $provider) {
				app()->resolveProviderClass($provider)->register();
			}
		}
	}

	/**
	 * Boots the module
	 */
	public function boot()
	{
		if (! $this->isEnabled()) {
			throw new RuntimeException("Module {$this->slug} cannot be booted until it has been enabled.");
		}

		// boot the module's service providers
		if (isset($this->attributes['providers'])) {
			foreach ($this->attributes['providers'] as $provider) {
				app()->resolveProviderClass($provider)->boot();
			}
		}

		$this->booted = true;

	}

	/**
	 * Returns the module's booted flag
	 *
	 * @return bool
	 */
	public function isBooted()
	{
		return (boolean) $this->booted;
	}

	/**
	 * Installs a module.
	 */
	public function install()
	{
		if ($this->isInstalled()) {
			return $this->entity;
		}

		$this->migrate();

		list($messages, $module) = $this->repository->create([
			'slug' => $this->slug,
			'version' => $this->attributes['version'],
			'installed' => true
		]);

		if ( ! $messages->isEmpty()) {
			throw new RuntimeException("An error occurred while installing the new module ({$this->slug}).");
		}

        // set the entity
        $this->entity = $module;

		return $module;
	}

	/**
	 * Returns the modules installed flag
	 *
	 * @return boolean
	 */
	public function isInstalled()
	{
		if ( empty($this->entity)) {
			return false;
		}

		return $this->entity->installed;
	}


	/**
	 * Enables a module
	 *
	 * @return mixed
	 */
	public function enable()
	{
        list($messages, $module) = $this->repository->updatedEnabled($this, ['enabled' => true]);

        if ( ! $messages->isEmpty()) {
            throw new RuntimeException("An error occurred while enabling the module ({$this->slug}).");
        }

		return [$messages, $module];
	}

	/**
	 * Returns true is the extension is enabled
	 *
	 * @return bool
	 */
	public function isEnabled()
	{
		if ( ! $this->isInstalled() || empty($this->entity)) {
			return false;
		}

		return (bool) $this->entity->enabled;
	}

	/**
	 * Enables a module
	 *
	 * @return mixed
	 */
	public function disable()
	{
		if ($this->isEnabled()) {

			list($messages, $module) = $this->repository->updatedEnabled($this, ['enabled' => false]);

			if ( ! $messages->isEmpty()) {
				throw new RuntimeException("An error occurred while installing the new module.");
			}
		}

		return [$messages, $module];

	}

	/**
	 * Returns true is the extension is not enabled
	 *
	 * @return bool
	 */
	public function isDisabled()
	{
		return (boolean) ! $this->isEnabled();
	}

	/**
	 * Returns the unique slug for the item
	 *
	 * @return mixed
	 */
	public function getSlug()
	{
		return $this->slug;
	}

	/**
	 * Returns an array of the item's dependencies
	 *
	 * @return mixed
	 */
	public function getDependencies()
	{
		if (isset($this->dependencies)) {
			return (array) $this->dependencies;
		}

		return [];
	}

	/**
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}


	/**
	 * @param string $path
	 */
	public function setPath($path)
	{
		$this->path = $path;
	}


	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}


	/**
	 * @param array $attributes
	 */
	public function setAttributes($attributes)
	{
		$this->attributes = $attributes;
	}

	/**
	 * Sets a given attribute for the extension.
	 *
	 * @param  string  $key
	 * @param  mixed  $value
	 * @return void
	 */
	public function setAttribute($key, $value)
	{
		$this->attributes[$key] = $value;
	}

	/**
	 * Returns the given attribute.
	 *
	 * @param  string  $key
	 * @param  mixed  $default
	 * @return mixed
	 */
	public function getAttribute($key, $default = null)
	{
		if (array_key_exists($key, $this->attributes))
		{
			return $this->attributes[$key];
		}
		return value($default);
	}


	/**
	 * @return string
	 */
	public function getEntity()
	{
		return $this->entity;
	}


	/**
	 * @param string $entity
	 */
	public function setEntity($entity)
	{
		$this->entity = $entity;
	}

	/**
	 * Returns the extension's attributes as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$attributes = array_except($this->attributes, $this->hiddenAttributes);

		$properties = [];

		foreach ($attributes as $key => $value)
		{
			$properties[$key] = $value;
		}

		return $properties;
	}

	/**
	 * Returns our extension's attributes as json
	 *
	 * @param null $options
	 * @return string
	 */
	public function toJson($options = null)
	{
		return json_encode($this->toArray(), $options);
	}


	/**
	 * Compares the installed version with the code version to
	 * see if the module needs to be upgraded.
	 *
	 * @return bool
	 */
	public function needsUpgrade()
	{
		if ( ! $this->isInstalled()) {
			return false;
		}

		return version_compare($this->version, $this->entity->version) > 0;
	}


	/**
	 * Upgrades the module by running migrations, seeders and
	 * updating the installed version in the database.
	 */
	public function upgrade()
	{
		if ( ! $this->isInstalled()) {
			throw new RuntimeException("Module {$this->module} is not installed and cannot be upgraded.");
		}

		$this->migrate();

		list($messages, $module) = $this->repository->updateVersion($this->slug, [
			'version' => $this->attributes['version'],
		]);

		if ( ! $messages->isEmpty()) {
			throw new RuntimeException("An error occurred while installing the new module.");
		}

		return $module;
	}

	/**
	 * Migrates our module's database tables
	 *
	 * @param null $path
	 */
	public function migrate($path = null)
	{
		$migrator = app('migrator');

		$path = $path ?: $this->getMigrationsPath();

		if (is_dir($path)) {
			$migrator->run($path);
		}
	}

	/**
	 * Returns the path to the module's migration files
	 *
	 * @return string
	 */
	public function getMigrationsPath()
	{
		return realpath($this->path . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR . 'migrations');
	}

	/**
	 * Reset the module's migrations.
	 *
	 * @param  string  $path
	 *
	 * @return void
	 */
	protected function resetMigrations($path = null)
	{
		$migrator = app('migrator');

		$path = $path ?: $this->getMigrationsPath();

		$files = $migrator->getMigrationFiles($path);
		$repository = $migrator->getRepository();

		// array of migrations that will be reset
		$migrations = array_intersect(array_reverse($repository->getRan()), $files);

		// Loop through the migrations to rollback
		foreach ($migrations as $migration) {

			// resolve the migration instance
			$instance = $migrator->resolve($migration);

			$instance->down();

			// delete thhe migration.
			$migrationClass = new StdClass;
			$migrationClass->migration = $migration;
			$repository->delete($migrationClass);
		}
	}

	/**
	 * Dynamically retrieve attributes on the object.
	 *
	 * @param  string  $key
	 * @return mixed
	 */
	public function __get($key)
	{
		return $this->getAttribute($key);
	}

	/**
	 * Dynamically set attributes on the object.
	 *
	 * @param  string  $key
	 * @param  mixed  $value
	 * @return void
	 */
	public function __set($key, $value)
	{
		$this->setAttribute($key, $value);
	}

	/**
	 * Determines if an attribute exists on the object.
	 *
	 * @param  string  $key
	 * @return boolean
	 */
	public function __isset($key)
	{
		return isset($this->attributes[$key]);
	}

	/**
	 * Unset an attribute on the object.
	 *
	 * @param  string  $key
	 * @return void
	 */
	public function __unset($key)
	{
		unset($this->attributes[$key]);
	}
}
