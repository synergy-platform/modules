<?php

namespace Synergy\Modules;

use Illuminate\Support\Collection;
use InvalidArgumentException;
use Synergy\Dependable\Sorter;

/**
 * Part of the Modules package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Modules
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/modules
 */

class Modules extends Collection
{

	/**
	 * Returns a collection of modules that are enabled
	 *
	 * @return static
	 */
	public function allEnabled()
	{
		$modules = $this->filter(function($module) {
			return $module->isEnabled();
		});

		return $modules;
	}

	/**
	 * Returns a collection of modules that are disabled
	 *
	 * @return static
	 */
	public function allDisabled()
	{
		$modules = $this->filter(function($module) {
			return $module->isDisabled();
		});

		return $modules;
	}

	/**
	 * Returns all disabled extensions
	 *
	 * @return static
	 */
	public function allBooted()
	{
		$modules = $this->filter(function($module)  {
			return $module->isBooted();
		});

		return $modules;
	}

	/**
	 * Returns all disabled extensions
	 *
	 * @return static
	 */
	public function allNotBooted()
	{
		$modules = $this->filter(function($module) {
			return ! $module->isBooted();
		});

		return $modules;
	}

	/**
	 * Finds and registers all modules found using the finder
	 */
	public function findAndRegister($sort = false)
	{
		foreach(app('modules.finder')->find() as $module)
		{
			$this->register($module);
		}

		if ($sort) {
			$this->sortByDependency();
		}
	}

	/**
	 * Registers our module
	 *
	 * @param $module
	 */
	public function register($module)
	{
		$module = $this->createInstance($module);

		$module->register();

		$this->items[$module->getSlug()] = $module;
	}

	/**
	 * Creates a new instance of our Module
	 *
	 * @param $module
	 * @return Module
	 */
	protected function createInstance($module)
	{
		$attributes = app('files')->getRequire($module);

		$messages = $this->validForRegistering($attributes);

		if ($messages->isEmpty()) {

			$path = dirname($module);

			$slug = $attributes['slug'];

			unset($attributes['slug']);


			return new Module(app('modules.repository'),$path, $slug, $attributes);
		}

		throw new InvalidArgumentException("Please verify the module manifest {$module} for correct attributes.");
	}

	/**
	 * Sorts our extensions based on dependencies
	 *
	 * @return void
	 */
	public function sortByDependency()
	{
		if (!empty($this->items))
		{
			$this->items = (new Sorter($this->all()))->order()->all();
		}
	}

	/**
	 * Validates the module manifest file
	 *
	 * @param $manifest
	 */
	public function validForRegistering($manifest)
	{
		$rules = [
			'slug' => 'required',
			'name' => 'required',
			'description' => 'required',
			'version' => 'required',
			'author.name' => 'required',
			'author.email' => 'required',
			'dependencies' => 'array',
			'providers' => 'array'
		];

		$validator = app('validator')->make($manifest, $rules);

		return $validator->errors();
	}
}
