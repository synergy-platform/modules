<?php

namespace Synergy\Modules\Repositories;

use Synergy\Modules\Models\Module as Model;
use Synergy\Modules\Module;
use Synergy\Modules\Validators\ModuleValidator;
use Synergy\Support\Traits\Repository;

/**
 * Part of the Modules package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Modules
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/Modules
 */

class ModuleRepository {

	use Repository;

	/**
	 * Name of the model to use
	 *
	 * @var string
	 */
	protected $model = 'Synergy\Modules\Models\Module';


	/**
	 * Creates a new instance of the Module repository
	 *
	 * @param ModuleValidator $validator
	 * @param null            $model
	 */
	public function __construct(ModuleValidator $validator, $model = null)
	{
		$this->validator = $validator;

		if ( ! is_null($model)) {
			$this->model = $model;
		}
	}

	/**
	 * Finds the module with the given id
	 *
	 * @param $id
	 */
	public function find($id)
	{
		if ($id instanceof Model) {
			return $id;
		}

		if ($id instanceof Module) {
			return $id->getEntity();
		}

		if (is_string($id) && str_contains($id, '/')) {
			return $this->findBySlug($id);
		}

		return $this->createModel()->find($id);
	}

	/**
	 * Returns the module with the given slug
	 *
	 * @param $slug
	 *
	 * @return mixed
	 */
	public function findBySlug($slug)
	{
		return $this
			->createModel()
			->newQuery()
			->whereSlug($slug)
			->first();
	}


	/**
	 * Ensures data is valid before creating
	 *
	 * @param $input
	 *
	 * @return \Illuminate\Validation\Validator
	 */
	public function validForCreation($input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * Ensures data is valid before updating
	 *
	 * @param $input
	 *
	 * @return \Illuminate\Validation\Validator
	 */
	public function validForUpdate($input)
	{
		return $this->validator->on('update')->validate($input);
	}

	/**
	 * Ensures data is valid before updating the enabled status
	 *
	 * @param $input
	 *
	 * @return \Illuminate\Validation\Validator
	 */
	public function validForUpdatingEnabledStatus($input)
	{
		return $this->validator->on('updateEnabled')->validate($input);
	}

	/**
	 * Ensures data is valid before updating the slug
	 *
	 * @param $input
	 *
	 * @return \Illuminate\Validation\Validator
	 */
	public function validForUpdatingSlug($input)
	{
		return $this->validator->on('updateSlug')->validate($input);
	}

	/**
	 * Ensures data is valid before updating the version
	 *
	 * @param $input
	 *
	 * @return \Illuminate\Validation\Validator
	 */
	public function validForUpdatingVersion($input)
	{
		return $this->validator->on('updateVersion')->validate($input);
	}

	/**
	 * Creates a new module
	 *
	 * @param $input
	 *
	 * @return array
	 */
	public function create($input)
	{
		$module = $this->createModel();

		$messages = $this->validForCreation($input);

		if ($messages->isEmpty()) {
			$module->create($input);
		}

		return [$messages, $module];
	}

	/**
	 * Updates a module
	 *
	 * @param       $id
	 * @param array $attributes
	 */
	protected function update($id, array $input = [])
	{
		$module = $this->find($id);

		$messages = $this->validForUpdate($input);

		if ($messages->isEmpty()) {
			$module->update($input);
		}

		return [$messages, $module];
	}

	/**
	 * Updates the module's installed version
	 *
	 * @param $id
	 * @param $version
	 *
	 * @return array
	 */
	public function updatedEnabled($id, $input)
	{
		return $this->updateItem($id, $input, 'enabled', 'validForUpdatingEnabledStatus');
	}

	/**
	 * Updates the module's installed version
	 *
	 * @param $id
	 * @param $version
	 *
	 * @return array
	 */
	public function updateInstalled($id, $input)
	{
		return $this->updateItem($id, $input, 'installed', 'validForUpdatingInstalled');
	}

	/**
	 * Updates the module's installed version
	 *
	 * @param $id
	 * @param $version
	 *
	 * @return array
	 */
	public function updatedSlug($id, $input)
	{
		return $this->updateItem($id, $input, 'slug', 'validForUpdatingSlug');
	}

	/**
	 * Updates the module's installed version
	 *
	 * @param $id
	 * @param $version
	 *
	 * @return array
	 */
	public function updateVersion($id, $input)
	{
		return $this->updateItem($id, $input, 'version', 'validForUpdatingVersion');
	}


	/**
	 * Method for updating attributes
	 *
	 * @param $id
	 * @param $input
	 * @param $item
	 * @param $validationMethod
	 *
	 * @return array
	 */
	protected function updateItem($id, $input, $item, $validationMethod)
	{
		$module = $this->find($id);

		$data = array_only($input, $item);
		unset($input);

		$messages = $this->{$validationMethod}($data);

		if ($messages->isEmpty()) {
			$module->update($data);
		}

		return [$messages, $module];
	}
}
