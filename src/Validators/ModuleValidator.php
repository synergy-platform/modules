<?php

namespace Synergy\Modules\Validators;

use Synergy\Support\Validator;

/**
 * Part of the Modules package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Modules
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/modules
 */

class ModuleValidator extends Validator {

	/**
	 * Array containing common validation rules
	 *
	 * @var array
	 */
	protected $rules = [
		'slug' => 'unique:modules',
		'version' => '',
		'installed' => 'boolean',
		'enabled' => 'boolean'
	];

	/**
	 * Rules used when validating a module's schema when
	 * registering it with the system
	 */
	public function onRegistering()
	{
		$this->rules['slug'] .= ',slug,{slug},slug|required';
		$this->rules['version'] .= '|required';
		$this->rules['description'] = '|required';
		$this->rules['author.name'] = '|required';
		$this->rules['author.email'] .= '|required';
	}

	/**
	 * Rules used when creating a new module
	 */
	public function onCreate()
	{
		$this->rules['slug'] .= '|required';
		$this->rules['version'] .= '|required';
	}

	/**
	 * Rules when updating a module's enabled status
	 */
	public function onUpdateSlug()
	{

		$this->rules = ['slug' => $this->rules['slug'] . '|required'];
	}

	/**
	 * Rules when updating a module's enabled status
	 */
	public function onUpdateEnabled()
	{
		$this->rules = ['enabled' => $this->rules['enabled'] . '|required'];
	}

	/**
	 * Rules when updating a module's installed status
	 */
	public function onUpdateInstalled()
	{
		$this->rules = ['installed' => $this->rules['installed'] . '|required'];
	}

	/**
	 * Rules when updating a module's version
	 */
	public function onUpdateVersion()
	{
		$this->rules = ['version' => $this->rules['version'] . '|required'];
	}
}
