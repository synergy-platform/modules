<?php

/**
 * Part of the Modules package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Modules
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/shanedaniels/modules
 */

if ( ! function_exists('modules')) {

	/**
	 * Get the path to the vendor folder.
	 *
	 * @param  string  $path
	 * @return string
	 */
	function modules($slug = null)
	{
		if ( ! is_null($slug)) {
			return app('modules')->get($slug);
		}

		return app('modules');
	}
}

if ( ! function_exists('modules_path')) {

	/**
	 * Get the path to the vendor folder.
	 *
	 * @param  string  $path
	 * @return string
	 */
	function modules_path($path = '')
	{
		return base_path('modules').($path ? DIRECTORY_SEPARATOR.$path : $path);
	}
}