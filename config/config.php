<?php

/**
 * Part of the Modules package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Modules
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/shanedaniels/modules
 */

return [

	/*
    |--------------------------------------------------------------------------
    | Paths
    |--------------------------------------------------------------------------
    |
    | Specify paths where the finder can locate modules.
    |
    */
	'paths' => [
		base_path('workbench'),
		modules_path()
	],

	/*
    |--------------------------------------------------------------------------
    | Manifest
    |--------------------------------------------------------------------------
    |
    | Specify the name of the module and the depth that the finder will search
	| when looking inside the module folders for the manifest file.
    |
    */
	'manifest' => [

		/*
		 * Name of the manifest file
		 */
		'name' => 'module.php',

		/*
		 * Depth that the finder will look inside directories for
		 * the manifest file.
		 */
		'depth' => 2,

		/*
		 * Rules used to validate the module's manifest file
		 */
		'rules' => [
			'slug' => 'required',
			'name' => 'required',
			'description' => 'required',
			'version' => 'required',
			'author.name' => 'required',
			'author.email' => 'required',
			'dependencies' => 'array',
			'providers' => 'array'
		]
	],

	/*
    |--------------------------------------------------------------------------
    | Auto Register
    |--------------------------------------------------------------------------
    |
    | Should modules be automatically loaded when the service provider loads.
    |
    */
	'register' => false,

	/*
    |--------------------------------------------------------------------------
    | Auto Boot
    |--------------------------------------------------------------------------
    |
    | Should modules be automatically booted once registered
    |
    */
	'boot' => false
];